import os
import re


def PlaceStepArguments(step):
	regex = "({[A-Za-z0-9_]*})"
	matches = re.findall(regex, step)

	for index, match in enumerate(matches):
		arg = "${%d:%s}" % ((index + 1), match[1:-1].strip())
		step = step.replace(match, arg)

	return step


def ExtractStep(line):
	if '"' in line:
		regex = '"([^"]*)"'
	elif "'" in line:
		regex = "'([^'']*)'"
	matches = re.findall(regex, line)
	step = matches[0]

	return step


def ExtractSteps(filename):
	f = open(filename)
	lines = f.readlines()
	f.close()

	steps = []

	for line in lines:
		if line.strip().startswith("@given") or line.strip().startswith("@Given"):
			step = ExtractStep(line)
			step = PlaceStepArguments(step)
			steps.append("Given " + step)
			steps.append("And " + step)
		elif line.strip().startswith("@when") or line.strip().startswith("@When"):
			step = ExtractStep(line)
			step = PlaceStepArguments(step)
			steps.append("When " + step)
			steps.append("And " + step)
		elif line.strip().startswith("@then") or line.strip().startswith("@Then"):
			step = ExtractStep(line)
			step = PlaceStepArguments(step)
			steps.append("Then " + step)
			steps.append("And " + step)

	return steps


def GenerateCompletionFile(stepfiles, outfile):
	content = []
	content.append('{\n')
	content.append('\t"scope": "source.feature",\n')
	content.append('\t"completions":\n')
	content.append('\t[\n')

	for stepfile in stepfiles:
		steps = ExtractSteps(stepfile)
		for step in steps[:-1]:
			content.append('\t\t"' + step + '",\n')
		content.append('\t\t"' + steps[-1] + '"\n') ## last item without ','

	content.append('\t]\n')
	content.append('}\n')

	textfile = open(outfile, "w")

	for line in content:
		textfile.write(line)

	textfile.close()
	print "Output file = %s" % outfile


stepfiles = ["./../../framework/CommonSteps.py"]
GenerateCompletionFile(stepfiles, "./../../sublime-plugin/Cylon.sublime-completions")