import os
import sys
import csv
import yaml
import glob

'''
 Use command:
 python MigratePageObject <path_to_csv_files>/*.csv
'''

def MigrateToYaml(csvfile):
    col = {'ObjectName':1,'Name':2,'LocatingMethod':3,'Locator':4,'PreCheck':5,'ObjectType':6,
        'DefaultState':7,'Image':8,'Link':9,'DefaultValue':10,'Lable':11,'AvailableValue':12, 'CheckAttribute':13}
    
    content = open(csvfile, "r")
    inputfile = csv.reader(content, delimiter=',', quotechar='"')

    page = {}
    elements = []

    for row in inputfile:
        if row[0] == "Name:":
            PageName = row[1]
            page['name'] = row[1]
        if row[0] == "Title:":
            PageTitle = row[1]
            page['title'] = row[1]
        if row[0] == "URL:":
            PageURL = row[1]
            page['url'] = row[1]

        if row[0] == "Element:":
            element = dict(
                name = row[col['ObjectName']],
                xpath = row[col['Locator']]
            )
            elements.append(element)

    data = dict(
        elements = elements,
        page = page
    )

    yamlfile = csvfile.replace(".csv", ".yaml")

    with open(yamlfile, 'w') as yaml_file:
        yaml_file.write('## %s\n' % page['name'])
        yaml_file.write('---\n')
        yaml_file.write(yaml.dump(data, default_flow_style=False))
        yaml_file.write('...')

def MigrateCsvPageObject(path):
    print "Converting csv files ..."
    for filename in glob.glob(path):
        MigrateToYaml(filename)
        print "Output file = %s" % filename.replace(".csv", ".yaml")
    return True

if __name__ == "__main__":
    path = sys.argv[1]
    MigrateCsvPageObject(path)
    